package com.example.demo.entity;

import static javax.persistence.FetchType.LAZY;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import com.example.demo.dto.TagDTO;

@Entity
@Table(name = "tags")
public class Tag {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="tag_id")
	private int id;
	
	@Column(name = "tag_name",unique = false, nullable = false)
	private String name;
	
	@ManyToMany(mappedBy = "tags", fetch = LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE})
	private Set<Post> posts = new HashSet<>();
	
	public Tag(TagDTO t) {
		this.name = t.getName();
	}

	public Tag(int id, String name, Set<Post> posts) {
		super();
		this.id = id;
		this.name = name;
		this.posts = posts;
	}

	public Tag() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Post> getPosts() {
		return posts;
	}

	public void setPosts(Set<Post> posts) {
		this.posts = posts;
	}

}
