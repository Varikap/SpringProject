package com.example.demo.service;

import java.util.List;
 
import com.example.demo.entity.User;

public interface UserServiceInterface {
	User findOne(Integer id);

	List<User> findAll();

	User save(User user);

	void remove(Integer id);
	
	User findByUsername(String username);

}
