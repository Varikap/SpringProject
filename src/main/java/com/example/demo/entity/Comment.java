package com.example.demo.entity;

import java.util.Date;

import javax.persistence.*;

import com.example.demo.dto.CommentDTO;
@Entity
@Table(name = "comments")
public class Comment {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="comment_id")
	private int id;
	
	@Column(name = "comment_title", unique = false, nullable = false)
	private String title;
	
	@Column(name = "comment_description", unique = false, nullable = false)
	private String description;
	
	@Column(name = "comment_date",unique = false, nullable = false)
	private Date date;
	
	@Column(name = "comment_likes",unique = false, nullable = false)
	private int likes;
	
	@Column(name = "comment_dislikes",unique = false, nullable = false)
	private int dislikes;
	
	@ManyToOne
	@JoinColumn(name="user_id", referencedColumnName ="user_id", nullable = false)
	private User user;
	
	@ManyToOne
	@JoinColumn(name="post_id", referencedColumnName ="post_id")
	private Post post;

	public Comment(int id, String title, String description, Date date, int likes, int dislikes, User user, Post post) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.date = date;
		this.likes = likes;
		this.dislikes = dislikes;
		this.user = user;
		this.post = post;
	}

	public Comment() {
	}
	
	public Comment(CommentDTO c) {
		this.title = c.getTitle();
		this.description = c.getDescription();
		this.date = new Date();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getLikes() {
		return likes;
	}

	public void setLikes(int likes) {
		this.likes = likes;
	}

	public int getDislikes() {
		return dislikes;
	}

	public void setDislikes(int dislikes) {
		this.dislikes = dislikes;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

}
