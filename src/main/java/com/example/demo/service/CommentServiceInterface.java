package com.example.demo.service;

import java.util.List;

import com.example.demo.entity.Comment;

public interface CommentServiceInterface {
	Comment findOne(Integer id);

	List<Comment> findAll();

	Comment save(Comment comment);

	void remove(Integer id);
	
	List<Comment> findByPost_Id(int id);
}
