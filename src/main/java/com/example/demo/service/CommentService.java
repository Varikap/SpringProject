package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Comment;
import com.example.demo.repository.CommentRepository;

@Service
public class CommentService implements CommentServiceInterface {

	@Autowired
	CommentRepository commentRepository;

	@Override
	public Comment findOne(Integer id) {
		return commentRepository.getOne(id);
	}

	@Override
	public List<Comment> findAll() {
		return commentRepository.findAll();
	}

	@Override
	public Comment save(Comment comment) {
		return commentRepository.save(comment);
	}

	@Override
	public void remove(Integer id) {
		commentRepository.deleteById(id);
	}

	@Override
	public List<Comment> findByPost_Id(int id) {
		return commentRepository.findByPost_Id(id);
	}

}
