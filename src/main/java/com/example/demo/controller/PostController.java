package com.example.demo.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.PostDTO;
import com.example.demo.dto.TagDTO;
import com.example.demo.entity.Post;
import com.example.demo.entity.Tag;
import com.example.demo.service.PostServiceInterface;
import com.example.demo.service.TagServiceInterface;
import com.example.demo.service.UserServiceInterface;


@RestController
@RequestMapping(value = "api/posts")
public class PostController {

	@Autowired
	private PostServiceInterface postService;

	@Autowired
	private TagServiceInterface tagService;

	@Autowired
	private UserServiceInterface userService;

	@GetMapping
	public ResponseEntity<List<PostDTO>> getAll() {
		List<Post> posts = postService.findAll();
		List<PostDTO> postsDTO = new ArrayList<PostDTO>();
		for (Post p : posts)
			postsDTO.add(new PostDTO(p));
		return new ResponseEntity<List<PostDTO>>(postsDTO, HttpStatus.OK);
	}

	@GetMapping(value = "/sort/date/asc")
	public ResponseEntity<List<PostDTO>> getAllDateAsc() {
		List<Post> posts = postService.findAllByOrderByDateAsc();
		List<PostDTO> postsDTO = new ArrayList<PostDTO>();
		for (Post p : posts)
			postsDTO.add(new PostDTO(p));
		return new ResponseEntity<List<PostDTO>>(postsDTO, HttpStatus.OK);
	}

	@GetMapping(value = "/sort/date/desc")
	public ResponseEntity<List<PostDTO>> getAllDateDesc() {
		List<Post> posts = postService.findAllByOrderByDateDesc();
		List<PostDTO> postsDTO = new ArrayList<PostDTO>();
		for (Post p : posts)
			postsDTO.add(new PostDTO(p));
		return new ResponseEntity<List<PostDTO>>(postsDTO, HttpStatus.OK);
	}

	@GetMapping(value = "/tags/{id}")
	public ResponseEntity<List<TagDTO>> getTagByPostId(@PathVariable("id") int id) {
		List<Tag> tags = tagService.findByPosts_Id(id);
		List<TagDTO> tagsDTO = new ArrayList<>();
		if (tags == null)
			return new ResponseEntity<List<TagDTO>>(HttpStatus.NOT_FOUND);
		for (Tag t : tags)
			tagsDTO.add(new TagDTO(t));
		return new ResponseEntity<List<TagDTO>>(tagsDTO, HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<PostDTO> getPost(@PathVariable("id") int id){
		Post post = postService.findOne(id);
		if (post == null)
			return new ResponseEntity<PostDTO>(HttpStatus.NOT_FOUND);
		return new ResponseEntity<PostDTO>(new PostDTO(post), HttpStatus.OK);
	}

	@PostMapping(value = "/create", consumes = "application/json")
	public ResponseEntity<PostDTO> savePost(@RequestBody PostDTO postDTO) {
		Post post = new Post(postDTO);
		post = postService.save(post);
		post.setUser(userService.findOne(postDTO.getUser().getId())); 
		return new ResponseEntity<PostDTO>(new PostDTO(post), HttpStatus.CREATED);
	}

	@PutMapping(value = "/{id}", consumes = "application/json")
	public ResponseEntity<PostDTO> updatePost(@PathVariable("id") int id, @RequestBody PostDTO postDTO) {
		Post post = postService.findOne(id);
		if (post == null)
			return new ResponseEntity<PostDTO>(HttpStatus.BAD_REQUEST);
		post.setTitle(postDTO.getTitle());
		post.setDescription(postDTO.getDescription());
		post.setDate(new Date());
		post.setLongitude(postDTO.getLongitude());
		post.setLatitude(postDTO.getLatitude());
		post = postService.save(post);
		return new ResponseEntity<PostDTO>(new PostDTO(post), HttpStatus.CREATED);
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Void> deletePost(@PathVariable("id") int id) {
		Post post = postService.findOne(id);
		if (post == null)
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		postService.remove(id);
		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}
}
