package com.example.demo.service;

import java.util.List;

import com.example.demo.entity.Tag;

public interface TagServiceInterface {
	Tag findOne(Integer id);

	List<Tag> findAll();

	Tag save(Tag tag);

	void remove(Integer id);
	
	List<Tag> findByPosts_Id(Integer postId);
}
