package com.example.demo.service;

import java.util.List;

import com.example.demo.entity.Post;

public interface PostServiceInterface {
	Post findOne(Integer id);

	List<Post> findAll();
	
	List<Post> findAllByOrderByDateDesc();
	
	List<Post> findAllByOrderByDateAsc();

	Post save(Post post);

	void remove(Integer id);
}
