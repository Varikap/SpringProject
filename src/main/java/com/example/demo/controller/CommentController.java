package com.example.demo.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.CommentDTO;
import com.example.demo.entity.Comment;
import com.example.demo.service.CommentServiceInterface;

@RestController
@RequestMapping(value = "api/comments")
public class CommentController {

	@Autowired
	private CommentServiceInterface commentService;

	@GetMapping
	public ResponseEntity<List<CommentDTO>> getAll() {
		List<Comment> comments = commentService.findAll();
		List<CommentDTO> commentsDTO = new ArrayList<>();
		for (Comment c : comments)
			commentsDTO.add(new CommentDTO(c));
		return new ResponseEntity<List<CommentDTO>>(commentsDTO, HttpStatus.OK);
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<CommentDTO> getComment(@PathVariable("id") int id) {
		Comment comment = commentService.findOne(id);
		if (comment == null)
			return new ResponseEntity<CommentDTO>(HttpStatus.NOT_FOUND);
		return new ResponseEntity<CommentDTO>(new CommentDTO(comment), HttpStatus.OK);
	}

	@GetMapping(value = "/post/{id}")
	public ResponseEntity<List<CommentDTO>> getCommentsByPostId(@PathVariable("id") int id) {
		List<Comment> comments = commentService.findByPost_Id(id);
		List<CommentDTO> commentsDTO = new ArrayList<>();
		for (Comment c : comments)
			commentsDTO.add(new CommentDTO(c));
		return new ResponseEntity<List<CommentDTO>>(commentsDTO, HttpStatus.OK);
	}

	@PostMapping(value = "/create", consumes = "application/json")
	public ResponseEntity<CommentDTO> saveComment(@RequestBody CommentDTO commentDTO) {
		Comment comment = new Comment(commentDTO);
		comment = commentService.save(comment);
		return new ResponseEntity<CommentDTO>(new CommentDTO(comment), HttpStatus.CREATED);
	}

	@PutMapping(value = "/{id}", consumes = "application/json")
	public ResponseEntity<CommentDTO> updateComment(@PathVariable("id") int id, @RequestBody CommentDTO commentDTO) {
		Comment comment = commentService.findOne(id);
		if (comment == null)
			return new ResponseEntity<CommentDTO>(HttpStatus.BAD_REQUEST);

		comment.setTitle(commentDTO.getTitle());
		comment.setDescription(commentDTO.getDescription());
		comment.setDate(new Date());
		comment = commentService.save(comment);
		return new ResponseEntity<CommentDTO>(new CommentDTO(comment), HttpStatus.CREATED);
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Void> deleteComment(@PathVariable("id") int id) {
		Comment comment = commentService.findOne(id);
		if (comment == null)
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		commentService.remove(id);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

}
