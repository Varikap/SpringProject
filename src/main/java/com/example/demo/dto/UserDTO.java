package com.example.demo.dto;

import com.example.demo.entity.User;

public class UserDTO {
	private int id;
	private String name;
	private String username;
	private String password;

	public UserDTO() { super(); }

	public UserDTO(int id, String name, String username, String password) {
		super();
		this.id = id;
		this.name = name;
		this.username = username;
		this.password = password;
	}

	public UserDTO(User u ) {
		this(u.getId(),u.getName(),u.getUsername(),u.getPassword());
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}
