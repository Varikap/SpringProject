package com.example.demo.dto;

import com.example.demo.entity.Tag;

public class TagDTO {
	private int id;
	private String name;
	
	public TagDTO() {
		super();
	}
	
	public TagDTO(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public TagDTO(Tag t) {
		this(t.getId(), t.getName());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
