package com.example.demo.dto;

import java.util.Date;

import com.example.demo.entity.Post;

public class PostDTO {
	private int id;
	private String title;
	private String description;
	private Date date;
	private int likes;
	private int dislikes;
	private double longitude;
	private double latitude;
	private UserDTO user;

	public PostDTO() {
		super();
	}

	public PostDTO(int id, String title, String description, Date date, int likes, int dislikes, double longitude,
			double latitude, UserDTO user) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.date = date;
		this.likes = likes;
		this.dislikes = dislikes;
		this.longitude = longitude;
		this.latitude = latitude;
		this.user = user;
	}

	public PostDTO(Post p) {
		this(p.getId(), p.getTitle(), p.getDescription(), p.getDate(), p.getLikes(), p.getDislikes(), p.getLongitude(),
				p.getLatitude(), new UserDTO(p.getUser()));
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getLikes() {
		return likes;
	}

	public void setLikes(int likes) {
		this.likes = likes;
	}

	public int getDislikes() {
		return dislikes;
	}

	public void setDislikes(int dislikes) {
		this.dislikes = dislikes;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}

}
