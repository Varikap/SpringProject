package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.TagDTO;
import com.example.demo.entity.Tag;
import com.example.demo.service.TagServiceInterface;

@RestController
@RequestMapping(value = "api/tags")
public class TagController {
	
	@Autowired
	private TagServiceInterface tagService;

	@GetMapping
	public ResponseEntity<List<TagDTO>> getAll() {
		List<Tag> tags = tagService.findAll();
		List<TagDTO> tagsDTO = new ArrayList<>();
		for (Tag t : tags)
			tagsDTO.add(new TagDTO(t));
		return new ResponseEntity<List<TagDTO>>(tagsDTO, HttpStatus.OK);
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<TagDTO> getTag(@PathVariable int id) {
		Tag tag = tagService.findOne(id);
		if (tag == null)
			return new ResponseEntity<TagDTO>(HttpStatus.NOT_FOUND); // TODO can't get 404 it spits 500 for some reason
		return new ResponseEntity<TagDTO>(new TagDTO(tag), HttpStatus.OK);
	}

	@PostMapping(value = "/create", consumes = "application/json")
	public ResponseEntity<TagDTO> createTag(@RequestBody TagDTO tagDTO) {
		Tag tag = new Tag(tagDTO);
		tag = tagService.save(tag);
		return new ResponseEntity<TagDTO>(new TagDTO(tag), HttpStatus.CREATED);
	}

	@PutMapping(value = "/{id}", consumes = "application/json")
	public ResponseEntity<TagDTO> updateTag(@PathVariable int id, @RequestBody TagDTO tagDTO) {
		Tag tag = tagService.findOne(id);
		if (tag == null)
			return new ResponseEntity<TagDTO>(HttpStatus.BAD_REQUEST);

		tag.setName(tagDTO.getName());
		tag = tagService.save(tag);
		return new ResponseEntity<TagDTO>(new TagDTO(tag), HttpStatus.CREATED);
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Void> deleteTag(@PathVariable("id") int id) {
		Tag tag = tagService.findOne(id);
		if (tag == null)
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		tagService.remove(id);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

}
