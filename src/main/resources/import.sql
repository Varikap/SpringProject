INSERT INTO users (user_name, user_password, user_username) VALUES ('a','a','a');
INSERT INTO users (user_name, user_password, user_username) VALUES ('b','b','b');

INSERT INTO posts (post_title, post_description, post_likes, post_date, post_dislikes, user_id, post_latitude, post_longitude) VALUES ('Prva vest', 'GNU', 11, '2017-11-11', 1, 2, 37.436227, -122.132592);
INSERT INTO posts (post_title, post_description, post_likes, post_date, post_dislikes, user_id, post_latitude, post_longitude) VALUES ('Druga vest', 'Linux', 66, '2015-11-11', 2, 2, 33.998768, -118.273992);
INSERT INTO posts (post_title, post_description, post_likes, post_date, post_dislikes, user_id, post_latitude, post_longitude) VALUES ('Treca vest', 'FreeBSD', 39, '2011-11-11', 3, 1, -22.524330, -41.946920);
INSERT INTO posts (post_title, post_description, post_likes, post_date, post_dislikes, user_id, post_latitude, post_longitude) VALUES ('Cetvrta vest', 'Arch', 1000, '2012-12-11', 1, 1, 35.707812, 139.724990);

INSERT INTO tags (tag_name) VALUES ('rms');
INSERT INTO tags (tag_name) VALUES ('free');
INSERT INTO tags (tag_name) VALUES ('source');
INSERT INTO tags (tag_name) VALUES ('hash');
INSERT INTO tags (tag_name) VALUES ('tag');

INSERT INTO comments (comment_title, comment_description, comment_likes, comment_dislikes, comment_date, post_id, user_id) VALUES ('Komentar prvi', 'Sadrzaj prvog komentara', 22, 11, '2010-11-11', 2, 1);
INSERT INTO comments (comment_title, comment_description, comment_likes, comment_dislikes, comment_date, post_id, user_id) VALUES ('Drugi komentar', 'Fin clanak', 5, 1, '2011-11-11', 3, 1);
INSERT INTO comments (comment_title, comment_description, comment_likes, comment_dislikes, comment_date, post_id, user_id) VALUES ('Treci komentar', 'bravo bravo', 3, 5, '2012-11-11', 4, 2);
INSERT INTO comments (comment_title, comment_description, comment_likes, comment_dislikes, comment_date, post_id, user_id) VALUES ('Cetvrti komentar', 'aaaaaaaaaaa', 5, 1, '2013-11-11', 3, 1);
INSERT INTO comments (comment_title, comment_description, comment_likes, comment_dislikes, comment_date, post_id, user_id) VALUES ('Komentarcic', 'bbbbbbbbb', 3, 5, '2014-11-11', 3, 2);

INSERT INTO post_tags (post_id, tag_id) VALUES (3, 1);
INSERT INTO post_tags (post_id, tag_id) VALUES (1, 2);
INSERT INTO post_tags (post_id, tag_id) VALUES (2, 3);
INSERT INTO post_tags (post_id, tag_id) VALUES (1, 4);
INSERT INTO post_tags (post_id, tag_id) VALUES (1, 5);